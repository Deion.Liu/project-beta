from common.json import ModelEncoder
from .models import Technician, ServiceAppointment, AutomobileVO

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "vin",
        "customer",
        "date_time",
        "reason",
        "technician",
        "id",
    ]
    encoders = { "technician": TechnicianEncoder() }

class AutoMobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]