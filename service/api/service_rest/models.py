from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=25, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name + " - " + str(self.employee_id)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    customer = models.CharField(max_length=100)
    date_time = models.DateTimeField()
    reason = models.TextField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    finished = models.BooleanField(default=False)

    def __str__(self):
        return self.vin + " = " + self.customer + " - " + self.reason

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
