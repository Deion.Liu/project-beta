from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, ServiceAppointment
import json

from .encoders import (
    TechnicianEncoder,
    AppointmentEncoder,
)

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder = TechnicianEncoder,
            safe=False
        )
    else:
        try:
            data = json.loads(request.body)
            technician = Technician.objects.create(**data)
            return JsonResponse(
                {"technicians": technician},
                encoder=TechnicianEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print("content", content)
        try:
            assigned_tech = content["technician_id"]
            print(assigned_tech)
            technician = Technician.objects.get(employee_id=assigned_tech)
            content["technician"] = technician
            print(content)
            app = ServiceAppointment.objects.create(**content)
        except ServiceAppointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid service appointment"},
                status=400,
            )
        return JsonResponse(
            app,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment(request, pk):
    if request.method == "GET":
        return JsonResponse(
            ServiceAppointment.objects.get(id=pk),
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        try:
            ServiceAppointment.objects.filter(id=pk).update(finished=True)
            service_appointment = ServiceAppointment.objects.filter(id=pk)
            return JsonResponse(
                service_appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse(
                {"message": "No such appointment"},
                status=400,
            )
    else: # DELETE
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET"])
def api_appointment_history(request, vin):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.get(vin=vin)
        return JsonResponse(
            appointments,
            encoder=AppointmentEncoder,
            safe=False,
        )