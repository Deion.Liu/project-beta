import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadData() {
  let manufacturers, models, autos, appointments;
  const response = await fetch("http://localhost:8100/api/manufacturers/");
  const response2 = await fetch("http://localhost:8100/api/models/");
  const response3 = await fetch("http://localhost:8100/api/automobiles/");
  if (response.ok) {
    const data = await response.json();
    console.log(data);
    manufacturers = data.manufacturers;
  } else {
    console.error(response);
  }

  if (response2.ok) {
    const data2 = await response2.json();
    console.log(data2);
    models = data2.models;
  } else {
    console.error(response2);
  }

  if (response3.ok) {
    const data3 = await response3.json();
    console.log(data3);
    autos = data3.autos;
  } else {
    console.error(response3);
  }

  root.render(
    <React.StrictMode>
      <App manufacturers={manufacturers} models={models} autos={autos} />
    </React.StrictMode>
  );
}
loadData();
