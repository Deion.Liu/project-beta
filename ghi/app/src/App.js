
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturerList from "./ManufacturerList";


import ManufacturerForm from "./ManufacturerForm";
import VehicleModelList from "./VehicleModelList";
import VehicleModelForm from "./VehicleModelForm";
import AutomobileList from "./AutomobileList";
import AutomobileForm from "./AutomobileForm";
import CustomerForm from './CustomerForm';
import SalesPersonForm from './SalesPersonForm';
import SalesRecordForm from './SalesRecordForm';
import SaleList from './SaleList';
import SalesPersonHistory from './SalePersonHistory';
import TechnicianForm from "./TechnicianForm";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import AppointmentHistory from "./AppointmentHistory";

function App(props) {
  if (props.manufacturers === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route
            path="manufacturers/"
            element={<ManufacturerList manufacturers={props.manufacturers} />}
          />
          <Route path="manufacturers/new/" element={<ManufacturerForm />} />
          <Route
            path="vehicle_models/"
            element={<VehicleModelList vehicleModels={props.models} />}
          />
          <Route path="vehicle_models/new/" element={<VehicleModelForm />} />
          <Route
            path="automobiles/"
            element={<AutomobileList automobiles={props.autos} />}
          />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="/sales_person" element={<SalesPersonForm />} />
          <Route path="/customer" element={<CustomerForm />} />
          <Route path="/sales_records/history" element={<SalesPersonHistory />} />
          <Route path="/sales_records/new" element={<SalesRecordForm />} />
          <Route path="/sales_records" element={<SaleList />} />

          <Route path="automobiles/new/" element={<AutomobileForm />} />
          <Route path="technicians/new/" element={<TechnicianForm />} />
          <Route
            path="appointments/"
            element={<AppointmentList appointments={props.appointments} />}
          />
          <Route path="appointments/new/" element={<AppointmentForm />} />
          <Route
            path="appointments/history/"
            element={<AppointmentHistory />}
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
