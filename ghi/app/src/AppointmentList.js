import React from "react";

class AppointmentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: [],
    };
    this.handleFinish = this.handleFinish.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      const appointmentsList = data.appointments.filter(
        (appointment) => !appointment.finished
      );
      this.setState({ appointments: appointmentsList });
    }
  }

  async handleFinish(appointment) {
    console.log(appointment);
    const url = `http://localhost:8080/api/appointments/${appointment}/`;
    const requestOption = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(url, requestOption);
    if (response.ok) {
      const data = await response.json();
      const app = data.service_appointment;
      const updatedList = [...this.state.appointments];
      let index = updatedList.indexOf(appointment);
      updatedList.splice(index, 1);
      console.log(updatedList);
      this.setState({ appointments: updatedList });
    }
  }

  async handleCancel(appointment) {
    console.log(appointment);
    const appointmentsUrl = `http://localhost:8080/api/appointments/${appointment}/`;
    const fetchConfig = {
      method: "DELETE",
      body: JSON.stringify(appointment),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentsUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);
    }
    const url = await fetch("http://localhost:8080/api/appointments/");
    if (url.ok) {
      const data = await url.json();
      this.setState({ appointments: data.appointments });
    }
  }

  render() {
    return (
      <>
        <h1>Service Appointments</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Appointment Date/Time</th>
              <th>Assigned Technician</th>
              <th>Reason</th>
              <th> </th>
            </tr>
          </thead>
          <tbody>
            {this.state.appointments.map((appointment) => {
              return (
                <tr key={appointment.vin}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.customer}</td>
                  <td>{appointment.date_time}</td>
                  <td>{appointment.technician.name}</td>
                  <td>{appointment.reason}</td>
                  <td>
                    <button
                      onClick={() => this.handleCancel(appointment.id)}
                      type="button"
                      className="btn btn-danger"
                    >
                      Cancel
                    </button>
                    <button
                      onClick={() => this.handleFinish(appointment.id)}
                      type="button"
                      className="btn btn-success"
                    >
                      Finished
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default AppointmentList;
