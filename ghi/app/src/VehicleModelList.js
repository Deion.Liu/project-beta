import React from "react";

function VehicleModelList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Photo</th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody>
        {props.vehicleModels.map((vehicleModel) => {
          return (
            <tr>
              <td>{vehicleModel.name}</td>
              <td>
                <img src={vehicleModel.picture_url} alt="Vehicle Model" />
              </td>
              <td>{vehicleModel.manufacturer.name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default VehicleModelList;
