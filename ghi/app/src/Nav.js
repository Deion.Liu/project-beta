import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">

            <li className="nav-item">
              <NavLink to="manufacturers/">List Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="manufacturers/new/">Create Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="vehicle_models/">List vehicle models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="vehicle_models/new/">Create vehicle model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="automobiles/">List automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="automobiles/new/">Create automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="sales_person/">Create Sales Person</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="customer/">Create Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="sales_records/new/">Create Sale Record</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="sales_records/">View Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="sales_records/history/">View Sales Person History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="technicians/new/">Create a technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="appointments/">All appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="appointments/new/">Schedule an appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="appointments/history/">Service history</NavLink>
            </li>


          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
