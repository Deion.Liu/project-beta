import React from "react";

class VehicleModelForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      picture_url: "",
      manufacturer_id: "",
      manufacturers: [],
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePhotoChange = this.handlePhotoChange.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(manufacturerUrl);

    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
    }
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }

  handlePhotoChange(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }

  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({ manufacturer_id: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    console.log(data);
    delete data.manufacturers;
    console.log(data);
    const vehicleModelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response2 = await fetch(vehicleModelUrl, fetchConfig);
    if (response2.ok) {
      const newVehicleModel = await response2.json();
      console.log(newVehicleModel);

      const cleared = {
        name: "",
        picture_url: "",
        manufacturer_id: "",
      };
      this.setState(cleared);
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new vehicle model</h1>
            <form onSubmit={this.handleSubmit} id="create-vehicle-form">
              <div className="form-floating mb-3">
                <input
                  value={this.state.name}
                  onChange={this.handleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={this.state.picture_url}
                  onChange={this.handlePhotoChange}
                  placeholder="Photo"
                  required
                  type="text"
                  name="photo"
                  id="photo"
                  className="form-control"
                />
                <label htmlFor="photo">Photo URL</label>
              </div>
              <div className="mb-3">
                <select
                  value={this.state.manufacturer_id}
                  name="manufacturer"
                  onChange={this.handleManufacturerChange}
                  required
                  id="manufacturer"
                  className="form-select"
                >
                  <option value="">Choose a manufacturer</option>
                  {this.state.manufacturers.map((manufacturer) => {
                    return (
                      <option key={manufacturer.name} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default VehicleModelForm;
