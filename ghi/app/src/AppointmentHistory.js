import React from "react";

class AppointmentHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      service_history: [],
    };
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ service_history: data.appointments });
    }
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  async handleSubmit(event) {
    event.preventDefault();

    const history = this.state.service_history.filter((service) => {
      return service.vin === this.state.vin;
    });
    this.setState({ service_history: history });
  }

  render() {
    return (
      <>
        <h1>Service History</h1>
        <form onSubmit={this.handleSubmit} id="appointments_history">
          <div className="main-search-input-wrap">
            <div className="main-search-input fl-wrap">
              <div className="main-search-input-item">
                <input
                  onChange={this.handleVinChange}
                  value={this.state.vin}
                  type="text"
                  placeholder="Search VIN"
                  requiredtype="text"
                  name="vin"
                  id="vin"
                />
              </div>
              <button className="main-search-button">Search</button>
            </div>
          </div>
        </form>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Appointment Date/Time</th>
              <th>Assigned Technician</th>
              <th>Service Reason</th>
            </tr>
          </thead>
          <tbody>
            {this.state.service_history
              ? this.state.service_history.map((history) => {
                  return (
                    <tr key={history.vin}>
                      <td>{history.vin}</td>
                      <td>{history.customer}</td>
                      <td>{history.date_time}</td>
                      <td>{history.technician.name}</td>
                      <td>{history.reason}</td>
                    </tr>
                  );
                })
              : null}
          </tbody>
        </table>
      </>
    );
  }
}

export default AppointmentHistory;
