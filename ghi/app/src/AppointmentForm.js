import React from "react";

class AppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      customer: "",
      date_time: "",
      reason: "",
      technician_id: "",
      technicians: [],
    };
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
    this.handleDateTimeChange = this.handleDateTimeChange.bind(this);
    this.handleReasonChange = this.handleReasonChange.bind(this);
    this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const techniciansUrl = "http://localhost:8080/api/technicians/";
    const response = await fetch(techniciansUrl);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({ customer: value });
  }

  handleDateTimeChange(event) {
    const value = event.target.value;
    this.setState({ date_time: value });
  }

  handleReasonChange(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }

  handleTechnicianChange(event) {
    const value = event.target.value;
    this.setState({ technician_id: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    console.log(data);
    delete data.technicians;
    console.log(data);
    const appointmentsUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentsUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);

      const cleared = {
        vin: "",
        customer: "",
        date_time: "",
        reason: "",
        technician_id: "",
      };
      this.setState(cleared);
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Schedule an appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input
                  value={this.state.vin}
                  onChange={this.handleVinChange}
                  placeholder="VIN"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={this.state.customer}
                  onChange={this.handleCustomerChange}
                  placeholder="Customer"
                  required
                  type="text"
                  name="customer"
                  id="customer"
                  className="form-control"
                />
                <label htmlFor="customer">Customer</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={this.state.date_time}
                  onChange={this.handleDateTimeChange}
                  placeholder="Date-time"
                  required
                  type="datetime-local"
                  name="date-time"
                  id="date-time"
                  className="form-control"
                />
                <label htmlFor="date-time">Date and time</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={this.state.reason}
                  onChange={this.handleReasonChange}
                  placeholder="Reason"
                  required
                  type="text"
                  name="reason"
                  id="reason"
                  className="form-control"
                />
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="mb-3">
                <select
                  value={this.state.technician_id}
                  name="technician_id"
                  onChange={this.handleTechnicianChange}
                  required
                  id="technician_id"
                  className="form-select"
                >
                  <option value="">Choose a technician</option>
                  {this.state.technicians.map((technician) => {
                    return (
                      <option
                        key={technician.employee_id}
                        value={technician.employee_id}
                      >
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentForm;
