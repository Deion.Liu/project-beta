# CarCar

Team:

- Deion - Automobile services
- Armen - Auto Sales

## Design

## Service microservice

Technician

- model: name, employee_id
- create encoder for technician
- add link to Nav.js
- add route to App.js
- create technician form
- write technician api's (get, post, put, delete)
- write paths and urls

Appointments

- model: vin, customer, date_time, reason, technician, finished
- create encoder for appointments
- add link to Nav.js
- add route to App.js
- create appointment form
- create appointment list(form)
- create appointment history list(form)
- write appointment api's (get, post, put, delete)
- write paths and urls

## Sales microservice


make models - 
SalesRecord: sales_person, automobile, , customer, saleprices
-Pick a vehicle - drop down list

-Pick a salesperson - drop down

-Pick a customer - drop down

-price of sale

SalesPerson: name, employee_number

Automobile: vin, import_href

Customer: name, address, phone_number

List of Sales will show Sales person, employee number, customer, vin, and sale price.

Nav links -

Create Sales Person, Create Customer, Create Sale Record, View sales, View sales history